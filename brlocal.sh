set -e

BUILDROOT_DIR=`pwd`/buildroot
LINUX_DIR=`pwd`/linux-xlnx
DT_EXTRA_DIR=`pwd`/device-tree-extra
OUTPUT_DIR=`pwd`/output

export BR2_EXTERNAL=`pwd`/buildroot_ext

# Need to make sure this is unset or buildroot will not work
unset LD_LIBRARY_PATH

cd $BUILDROOT_DIR

make ultra96local_defconfig

# Use the linux source in the submodule
echo "LINUX_OVERRIDE_SRCDIR=$LINUX_DIR" > $BR2_EXTERNAL/local.mk
echo "AXIDMALOOPBACK_OVERRIDE_SRCDIR=/home/bashton/repos/axidmasimple-kernel" >> $BR2_EXTERNAL/local.mk
sed -i "/^BR2_PACKAGE_OVERRIDE_FILE=.*/c\BR2_PACKAGE_OVERRIDE_FILE=\"$BR2_EXTERNAL\/local.mk\"" .config

# Increase the number of parallel jobs. Build root does not support make -jN
sed -i "/^BR2_JLEVEL=.*/c\BR2_JLEVEL=4" .config

cd $LINUX_DIR
git checkout -- $LINUX_DIR/arch/arm64/boot/
# copy over new device tree files.
cp -r $DT_EXTRA_DIR/* $LINUX_DIR/arch/arm64/boot/dts/xilinx/

# We can make just the dtb
make ARCH=arm64 CROSS_COMPILE=aarch64-linux-gnu- xilinx/custom_ultra96.dtb
cp $LINUX_DIR/arch/arm64/boot/dts/xilinx/custom_ultra96.dtb $OUTPUT_DIR/system-top.dtb


cd $BUILDROOT_DIR
#make linux-dirclean
make