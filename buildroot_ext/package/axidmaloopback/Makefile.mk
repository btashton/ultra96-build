################################################################################
#
# axidmaloopback
################################################################################

AXIDMALOOPBACK_VERSION = 0.1
AXIDMALOOPBACK_SITE = https://gitlab.com/btashton/axidmasimple-kernel
AXIDMALOOPBACK_SITE_METHOD = git
AXIDMALOOPBACK_LICENSE = MIT GPL-2.0+
AXIDMALOOPBACK_LICENSE_FILES = LICENSE.GPL LICENSE.MIT

$(eval $(kernel-module))
$(eval $(generic-package))