# Build Environment For Ultra96 Board

__Warning:__ This is a work in progress and files and structure may change
frequently,

## Usage

Right now this heavily depends on using the GitLab build system. Looking at
`.gitlab-ci.yml` will give you a good idea about how it is all wired together.

The short of it is to just have the current minimal system running, grab the
build artifacts from master.  [Download](https://gitlab.com/btashton/ultra96-build/-/jobs/artifacts/master/download?job=package_fit)

From here grab these files:
 * `BOOT.bin`
 * `uenv.txt`
 * `fitImage.itb`

and copy them to an SD card with a FAT partition
as the first partition.

Place the SD card in the Ultra96 board and boot.  On the UART connected to J6
you should see build root booting.

## Local Kernel and RootFS build

Make sure to checkout the `buildroot` and `linux-xlnx` submodules.  Then the 
`./brlocal.sh` script can be run from the repository root directory.  This
will perform a build using the buildroot config in
`buildroot_ext/configs/ultra96local_defconfig`.  This also uses a kernel
defconfig located at `buildroot_ext/kernel/ultra96_defconfig`.  The fitiamge
will still need to be recreated using the kernel and rootfs images that
buildroot generates.

## Known Issues

 * Bluetooth: Complains that the it cannot load the firmware file sometimes:
    ```
    [   22.380218] Bluetooth: hci0 command 0xff05 tx timeout
    [   30.444264] Bluetooth: hci0: send command failed
    [   30.448888] Bluetooth: hci0: download firmware failed, retrying...
    [   30.991380] Bluetooth: hci0: change remote baud rate command in firmware
    ```
   Unclear what is causing this.


# Networking

The gadget interface can be created by simply issuing `modprobe g_ether`

Then on the host `ifconfig usb0 10.0.1.1 netmask 255.255.255.0`

Then on the guest `ifconfig usb0 10.0.1.2 netmask 255.255.255.0`
