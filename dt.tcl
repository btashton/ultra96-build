set TMPWRK tmpwrk
setws ${TMPWRK}

repo -set device-tree-xlnx
repo -scan

createhw -name hw_0 -hwspec system.hdf
createbsp -name device_tree -hwproject hw_0 -proc psu_cortexa53_0 -os device_tree

configbsp -bsp device_tree console_device  "psu_uart_1"
updatemss -mss ${TMPWRK}/device_tree/system.mss
regenbsp -bsp device_tree
