set -e

BUILDROOT_DIR=`pwd`/buildroot
OUTPUT_DIR=`pwd`/output
BUILD_DIR=`pwd`/tmp_builddir

mkdir -p $BUILD_DIR
cp fitImage.its $BUILD_DIR/
cp $OUTPUT_DIR/system-top.dtb $BUILD_DIR
#cp $BUILDROOT_DIR/output/images/custom_ultra96.dtb $BUILD_DIR/system-top.dtb
cp $BUILDROOT_DIR/output/images/Image $BUILD_DIR
cp $BUILDROOT_DIR/output/images/rootfs.cpio $BUILD_DIR
PATH=$PATH:/$OUTPUT_DIR/dtc:/$OUTPUT_DIR/uboot_tools/ mkimage -f $BUILD_DIR/fitImage.its $OUTPUT_DIR/fitImage.itb
