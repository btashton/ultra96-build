set TMPWRK tmpwrk
setws ${TMPWRK}

createhw -name hw_0 -hwspec system.hdf

createapp -name fsbl -app {Zynq MP FSBL} -hwproject hw_0 -proc psu_cortexa53_0 -os standalone

configbsp -bsp fsbl_bsp stdin  "psu_uart_1"
configbsp -bsp fsbl_bsp stdout "psu_uart_1"
updatemss -mss ${TMPWRK}/fsbl_bsp/system.mss
regenbsp -bsp fsbl_bsp

projects -clean
projects -build
