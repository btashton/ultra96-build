set TMPWRK tmpwrk
setws ${TMPWRK}

createhw -name hw_0 -hwspec system.hdf

createapp -name pmufw -app {ZynqMP PMU Firmware} -hwproject hw_0 -proc psu_pmu_0 -os standalone

configbsp -bsp pmufw_bsp stdin  "psu_uart_1"
configbsp -bsp pmufw_bsp stdout "psu_uart_1"
updatemss -mss ${TMPWRK}/pmufw_bsp/system.mss
regenbsp -bsp pmufw_bsp

projects -clean
projects -build
